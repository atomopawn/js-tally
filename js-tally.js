/** Copyright 2020 Dr. Robert Marmorstein

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. 
*/

/** 
 * The Tally object represents a single group of tally marks which should be
 * drawn inside an SVG object.
 *   @param {object} svg The parent SVG object within which the tally marks will
 *   be drawn.  The width and height of svg should be set and the viewBox should be
 *   set to extend from (0,0) to (width, height).
 */  

function Tally(parent, width) {
    const svgns = "http://www.w3.org/2000/svg";

    if (width == undefined) {
        width = 300;
    }

    this.marks = 0;
    this.width = width;
    this.maxgroups = Math.floor((this.width-20)/45) - 1;
    this.color="black";
    this.slash_color="black";

    this.viewbox = "0 0 " + this.width + " 50";

    this.svg = $(document.createElementNS(svgns, "svg")).attr({
        "width" : this.width,
        "height" : 50,
        "viewBox" : this.viewbox,
        "xmlns" : svgns
    });
    parent.append(this.svg);

    this.addMark = function() {
        new_mark = document.createElementNS(svgns, "line");    
        let fives = Math.floor(this.marks / 5);
        levels = Math.floor(fives/this.maxgroups);
        let ones = this.marks % 5;
        let top = 10 + levels*40;
        let start = 10 + Math.floor(fives % this.maxgroups)*45;

        if (ones == 4) {
            new_mark.setAttributeNS(null, "x1", start-6);
            new_mark.setAttributeNS(null, "x2", start+30);
            new_mark.setAttributeNS(null, "y1", top);
            new_mark.setAttributeNS(null, "y2", top+30);
            new_mark.setAttributeNS(null, "stroke", this.slash_color); 
        }
        else {
            x = 9*ones + start;
            new_mark.setAttributeNS(null, "y1", top);
            new_mark.setAttributeNS(null, "y2", top+30);
            new_mark.setAttributeNS(null, "x1", x);
            new_mark.setAttributeNS(null, "x2", x);
            new_mark.setAttributeNS(null, "stroke", this.color); 
        }
        new_mark.setAttributeNS(null, "stroke-width", "3"); 
        this.svg.append(new_mark);

        ++this.marks;
        newheight = (levels + 1) * 50;
        if (this.svg.height() < newheight) {
            this.svg.attr("viewBox", "0 0 " + this.width + " " + newheight);
            this.svg.attr("height", newheight);
        }
    }
    this.setColor = function(color,slash_color) {
        this.color = color;
        if (slash_color == undefined)
            this.slash_color = color;
        else {
            this.slash_color = slash_color;
        }
    }
}

/* vim: set ts=4 expandtab sw=4 sts=4: */
