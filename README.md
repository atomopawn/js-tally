# js-tally

js-tally is a Javascript Library for drawing tally marks using SVG.  It
provides a "Tally" object that represents a set of talley marks within a
container of given width.  Tally marks can be added one at a time and
automatically wrap to new rows based on the width of the tally group.  
The color of the marks can also be adjusted.

## Requirements

js-tally relies on JQuery.  It is known to work with JQuery v3.5.1 and later,
but probably also works with earlier versions.

## Installation

To install, download the file "tallies.js" into your project folder and pull it
in to your project using a &lt;script&gt; tag:

&lt;script src="js-tally.js"&gt;&lt;/script&gt;

This must come AFTER the script tag that loads JQuery.

## Usage

let parent = $("div#somediv");

let tally = new Tally(parent);              # Creates a Tally with the default width of 300 pixels and adds it to parent
tally.svg.click(tally.addMark.bind(tally)); # Binds a click event to the addMark function
tally.setColor("black", "red");             # Set the straight marks to black and the "slash" color to red

let tally2 = new Tally(parent, 600);           # Create a Tally of width 600 pixels and add it to the parent.
tally2.svg.click(tally2.addMark.bind(tally2)); # Bind a click event to the addMark function
tally2.setColor("blue");                       # Set the color of all marks (including the slash) to blue

## License
[MIT](https://choosealicense.com/licenses/mit/)
